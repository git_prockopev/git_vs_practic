#include <iostream>
#include <string>
using namespace std;

//Practical task 19.5

class Animal
{
public:
	virtual void Voice()
	{
		std::cout << "Silence";
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Meow!";
	}
};

class Cow : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Moo!";
	}
};

class Dog : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Woof!";
	}
};

int main()
{
	Cat cat;
	Cow cow;
	Dog dog;

	Animal* animals[3] = { &cat, &cow, &dog };
	for (int i = 0; i < 3; i++)
	{
		animals[i]->Voice();
		std::cout << "\n";
	}

	std::cin;
	return 0;
}